#include <stdio.h>
#include <mpi.h>


int main(int argc, char *argv[])
{
    /* declare any variables you need */
    int my_rank;
    int size;

    MPI_Init(&argc, &argv);

    /* Get the rank of each process */
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    /* Get the size of the communicator */
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (my_rank == 0) {
        printf ("Hello world!\n");
    }

    printf("I am process %i out of %i.\n", my_rank, size);

    MPI_Finalize();
    return 0;
}
